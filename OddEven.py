
while True:
    try:
        number = int(input("Enter a number: "))
        break
    except ValueError:
        print("Please enter an integer!")

if number % 2 == 0:
    if number % 4 == 0:
        print("%d is even and divisable by 4" % number)
    else:
        print("%d is an even number" % number)
else:
    print("%d is an odd number" % number)