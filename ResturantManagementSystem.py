from tkinter import *
import time
import random

root = Tk()
root.geometry("1600x800+0+0")
root.title("Restaurant Management System")

text_input = StringVar()
reference = StringVar()
text1 = StringVar()
text2 = StringVar()
text3 = StringVar()
text4 = StringVar()
text5 = StringVar()
text6 = StringVar()
text7 = StringVar()

text8 = StringVar()
text9 = StringVar()
text10 = StringVar()
text11 = StringVar()
text12 = StringVar()
text13 = StringVar()
operator = ""


def btnclick(number):
    global operator
    operator = operator + str(number)
    text_input.set(operator)


def btnclear():
    global operator
    operator = ""
    text_input.set("")


def btnequal():
    global operator
    action = str(eval(operator))
    text_input.set(action)
    operator = ""


def total():
    txtref.config(state=NORMAL)
    txt11.config(state=NORMAL)
    txt12.config(state=NORMAL)
    txt13.config(state=NORMAL)
    x = str(random.randint(60000, 99999))
    reference.set(x)

    costoftxt1 = float(text1.get()+'0')*2
    costoftxt2 = float(text2.get()+'0')*3
    costoftxt3 = float(text3.get()+'0')*4

    costofmeal = (costoftxt1 + costoftxt2 + costoftxt3)
    c_costofmeal = '€', str('%.2f' % costofmeal)
    text11.set(c_costofmeal)

    tax = costofmeal*.18
    c_tax = '€', str('%.2f' % tax)
    text12.set(c_tax)

    totalcost = tax + costofmeal
    c_total = '€', str('%.2f' % totalcost)
    text13.set(c_total)


def reset():
    txtref.config(state=DISABLED)
    txt11.config(state=DISABLED)
    txt12.config(state=DISABLED)
    txt13.config(state=DISABLED)

    reference.set("")
    text1.set("")
    text2.set("")
    text3.set("")
    text4.set("")
    text5.set("")
    text6.set("")
    text7.set("")
    text8.set("")
    text9.set("")
    text10.set("")
    text11.set("")
    text12.set("")
    text13.set("")


# ________________________________Frames___________________________________
Tops = Frame(root, width=1600, height=50, bg="powder blue", relief=SUNKEN)
Tops.pack(side=TOP)

f1 = Frame(root, width=800, height=700, relief=SUNKEN)
f1.pack(side=LEFT)

f2 = Frame(root, width=300, height=700, relief=SUNKEN)
f2.pack(side=RIGHT)

# ________________________________Title___________________________________
localtime = time.asctime(time.localtime(time.time()))
lblInfo = Label(Tops, font=('arial', 50, 'bold'), text="Restaurant Management System", fg="steel blue", anchor='w')
lblInfo.grid(column=0, row=0)
lblInfo = Label(Tops, font=('arial', 20, 'bold'), text=localtime, fg="steel blue", anchor='w')
lblInfo.grid(column=0, row=1)

# ________________________________Calculator___________________________________
display = Entry(f2, font=('arial', 20, 'bold'), textvariable=text_input, bd=30, insertwidth=4, bg="powder blue", justify='right')
display.grid(columnspan=4)

btn7 = Button(f2, padx=16, pady=16, bd=8, font=('arial', 20, 'bold'), text="7", command=lambda: btnclick(7), bg="powder blue").grid(row=2, column=0)
btn8 = Button(f2, padx=16, pady=16, bd=8, font=('arial', 20, 'bold'), text="8", command=lambda: btnclick(8), bg="powder blue").grid(row=2, column=1)
btn9 = Button(f2, padx=16, pady=16, bd=8, font=('arial', 20, 'bold'), text="9", command=lambda: btnclick(9), bg="powder blue").grid(row=2, column=2)
btnd = Button(f2, padx=16, pady=16, bd=8, font=('arial', 20, 'bold'), text="/", command=lambda: btnclick("/"), bg="powder blue").grid(row=2, column=3)
btn4 = Button(f2, padx=16, pady=16, bd=8, font=('arial', 20, 'bold'), text="4", command=lambda: btnclick(4), bg="powder blue").grid(row=3, column=0)
btn5 = Button(f2, padx=16, pady=16, bd=8, font=('arial', 20, 'bold'), text="5", command=lambda: btnclick(5), bg="powder blue").grid(row=3, column=1)
btn6 = Button(f2, padx=16, pady=16, bd=8, font=('arial', 20, 'bold'), text="6", command=lambda: btnclick(6), bg="powder blue").grid(row=3, column=2)
btnm = Button(f2, padx=16, pady=16, bd=8, font=('arial', 20, 'bold'), text="*", command=lambda: btnclick("*"), bg="powder blue").grid(row=3, column=3)
btn3 = Button(f2, padx=16, pady=16, bd=8, font=('arial', 20, 'bold'), text="3", command=lambda: btnclick(3), bg="powder blue").grid(row=4, column=0)
btn2 = Button(f2, padx=16, pady=16, bd=8, font=('arial', 20, 'bold'), text="2", command=lambda: btnclick(2), bg="powder blue").grid(row=4, column=1)
btn1 = Button(f2, padx=16, pady=16, bd=8, font=('arial', 20, 'bold'), text="1", command=lambda: btnclick(1), bg="powder blue").grid(row=4, column=2)
btna = Button(f2, padx=16, pady=16, bd=8, font=('arial', 20, 'bold'), text="+", command=lambda: btnclick("+"), bg="powder blue").grid(row=4, column=3)
btnc = Button(f2, padx=16, pady=16, bd=8, font=('arial', 20, 'bold'), text="C", command=btnclear, bg="powder blue").grid(row=5, column=0)
btn0 = Button(f2, padx=16, pady=16, bd=8, font=('arial', 20, 'bold'), text="0", command=lambda: btnclick(0), bg="powder blue").grid(row=5, column=1)
btne = Button(f2, padx=16, pady=16, bd=8, font=('arial', 20, 'bold'), text="=", command=btnequal, bg="powder blue").grid(row=5, column=2)
btns = Button(f2, padx=16, pady=16, bd=8, font=('arial', 20, 'bold'), text="-", command=lambda: btnclick("-"), bg="powder blue").grid(row=5, column=3)

# ________________________________Information 1___________________________________
lblref = Label(f1, font=('arial', 16, 'bold'), text="References", bd=16, anchor='w')
lblref.grid(row=0, column=0)
txtref = Entry(f1, font=('arial', 20, 'bold'), textvariable=reference, insertwidth=4, justify='right', bd=10, bg="powder blue")
txtref.grid(row=0, column=1)
txtref.config(state=DISABLED)
lbl1 = Label(f1, font=('arial', 16, 'bold'), text="lbl1", bd=16, anchor='w')
lbl1.grid(row=1, column=0)
txt1 = Entry(f1, font=('arial', 20, 'bold'), textvariable=text1, insertwidth=4, justify='right', bd=10, bg="powder blue")
txt1.grid(row=1, column=1)
lbl2 = Label(f1, font=('arial', 16, 'bold'), text="txt2", bd=16, anchor='w')
lbl2.grid(row=2, column=0)
txt2 = Entry(f1, font=('arial', 20, 'bold'), textvariable=text2, insertwidth=4, justify='right', bd=10, bg="powder blue")
txt2.grid(row=2, column=1)
lbl3 = Label(f1, font=('arial', 16, 'bold'), text="txt3", bd=16, anchor='w')
lbl3.grid(row=3, column=0)
txt3 = Entry(f1, font=('arial', 20, 'bold'), textvariable=text3, insertwidth=4, justify='right', bd=10, bg="powder blue")
txt3.grid(row=3, column=1)
lbl4 = Label(f1, font=('arial', 16, 'bold'), text="txt4", bd=16, anchor='w')
lbl4.grid(row=4, column=0)
txt4 = Entry(f1, font=('arial', 20, 'bold'), textvariable=text4, insertwidth=4, justify='right', bd=10, bg="powder blue")
txt4.grid(row=4, column=1)
lbl5 = Label(f1, font=('arial', 16, 'bold'), text="txt5", bd=16, anchor='w')
lbl5.grid(row=5, column=0)
txt5 = Entry(f1, font=('arial', 20, 'bold'), textvariable=text5, insertwidth=4, justify='right', bd=10, bg="powder blue")
txt5.grid(row=5, column=1)

# ________________________________Information 1___________________________________
lbl8 = Label(f1, font=('arial', 16, 'bold'), text="lbl8", bd=16, anchor='w')
lbl8.grid(row=0, column=2)
txt8 = Entry(f1, font=('arial', 20, 'bold'), textvariable=lbl8, insertwidth=4, justify='right', bd=10, bg="powder blue")
txt8.grid(row=0, column=3)
lbl9 = Label(f1, font=('arial', 16, 'bold'), text="lbl9", bd=16, anchor='w')
lbl9.grid(row=1, column=2)
txt9 = Entry(f1, font=('arial', 20, 'bold'), textvariable=lbl9, insertwidth=4, justify='right', bd=10, bg="powder blue")
txt9.grid(row=1, column=3)
lbl10 = Label(f1, font=('arial', 16, 'bold'), text="txt10", bd=16, anchor='w')
lbl10.grid(row=2, column=2)
txt10 = Entry(f1, font=('arial', 20, 'bold'), textvariable=lbl10, insertwidth=4, justify='right', bd=10, bg="powder blue")
txt10.grid(row=2, column=3)
lbl11 = Label(f1, font=('arial', 16, 'bold'), text="Total", bd=16, anchor='w')
lbl11.grid(row=3, column=2)
txt11 = Entry(f1, font=('arial', 20, 'bold'), textvariable=text11, insertwidth=4, justify='right', bd=10, bg="powder blue")
txt11.grid(row=3, column=3)
txt11.config(state=DISABLED)
lbl12 = Label(f1, font=('arial', 16, 'bold'), text="Tax", bd=16, anchor='w')
lbl12.grid(row=4, column=2)
txt12 = Entry(f1, font=('arial', 20, 'bold'), textvariable=text12, insertwidth=4, justify='right', bd=10, bg="powder blue")
txt12.grid(row=4, column=3)
txt12.config(state=DISABLED)
lbl13 = Label(f1, font=('arial', 16, 'bold'), text="Net Total", bd=16, anchor='w')
lbl13.grid(row=5, column=2)
txt13 = Entry(f1, font=('arial', 20, 'bold'), textvariable=text13, insertwidth=4, justify='right', bd=10, bg="powder blue")
txt13.grid(row=5, column=3)
txt13.config(state=DISABLED)

# ________________________________Buttons___________________________________
btntotal = Button(f1, font=('arial', 20, 'bold'), padx=16, pady=8, text="Total", bg="powder blue", bd=10, command=total)
btntotal.grid(row=7, column=1)
btnreset = Button(f1, font=('arial', 20, 'bold'), padx=16, pady=8, text="Reset", bg="powder blue", bd=10, command=reset)
btnreset.grid(row=7, column=2)
btnexit = Button(f1, font=('arial', 20, 'bold'), padx=16, pady=8, text="Exit", bg="powder blue", bd=10, command=exit)
btnexit.grid(row=7, column=3)

"""
lbl = Label(f1, font=('arial', 16, 'bold'), text="", bd=16, anchor='w')
lbl.grid(row=, column=0)
txt = Entry(f1, font=('arial', 20, 'bold'), textvariable=, insertwidth=4, justify='right', bd=10, bg="powder blue")
txt.grid(row=, column=1)
"""

root. mainloop()

