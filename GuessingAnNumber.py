import random

print("Welcome to guessing number.")
print("You have three chances to guess one number")


def userTryF():
    while True:
        try:
            userTry = int(input("How many games, do you want to play: \n"))
            break
        except ValueError:
            print("please enter an integer number")
    return userTry


trial = 1
score = 0
userTry = userTryF()
turn = 1
while userTry > 0:
    x = random.randint(1, 9)
    while turn < 4:
        while True:
            try:
                userInput = int(input("%d.%d Guess the number(1-9): " % (trial, turn)))
                break
            except ValueError:
                print("Please a number from 1 to 9")

        if userInput == x:
            print("______same______")
            score += 1
            turn = 4
        elif userInput < x:
            print("\tGuess higher")
        else:
            print("\tGuess lower")
        turn += 1
    turn = 1
    print("\tNumber = %d\n" % x)
    if userTry == 1:
        print("\nScore: %d/%d" % (score, trial))

        yesNo = str(input("This is your last game. do you like to continue(y/n): ")).lower()
        z = 1
        while z > 0:
            if yesNo == 'y':
                userTry = userTryF() + 1
                trial = 0
                z = 0
                score = 0
            elif yesNo == 'n':
                break
            else:
                yesNo = str(input("This is your last game. do you like to continue(y/n): ")).lower()

    trial += 1
    userTry -= 1
